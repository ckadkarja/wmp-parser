package com.wmp.parser.common.util;

import java.util.Arrays;
import java.util.Comparator;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.wmp.parser.beans.ParserResultVO;
import com.wmp.parser.common.consts.WmpConstant;

public class StringUtil {
	private static final Logger logger = LoggerFactory.getLogger(StringUtil.class);
	
	public static final String URL_REGEX = "^(https|http?):\\/\\/([^:\\/\\s]+)(:([^\\/]*))?((\\/[^\\s/\\/]+)*)?\\/?([^#\\s\\?]*)(\\?([^#\\s]*))?(#(\\w*))?$";
	public static final String PARSER_REGEX = "[^0-9a-zA-Z]";
	public static final String INT_REGEX = "[0-9]";
	
	public static boolean isUrlFormat(String url) throws Exception {
		Pattern p = Pattern.compile(URL_REGEX);
		Matcher m = p.matcher(url);
		
		return m.matches();
    }
	
	/**
	 * 텍스트에서 묶음 단위로 몫,나머지 값 추출 하여 전달
	 * --> 소팅까지 진행
	 * --> 전체묶음에서 소팅|부분묶음세서 소팅 후 합치기
	 * @param webSource
	 * @param unitBind
	 * @param bindType
	 * @return ParserResultVO (몫, 나머지)
	 * @throws Exception
	 */
	public static ParserResultVO getExtractedValue(String text, int unitBind, String bindType) throws Exception {
		
		if (text == null) {
			throw new Exception("text value check..!");
		}
		
		int textLength = text.length();
		int remainLength = textLength % unitBind;

		String shareText = text.substring(0, (textLength - remainLength));
		String remainText = text.substring((textLength - remainLength), textLength);
		String sortedText = "";
		
		if (textLength != remainLength) {
			
			if (WmpConstant.PARSER_ALL.equals(bindType)) {
				sortedText = sortText(shareText);
			} else {
				
				int share = textLength / unitBind;
				StringBuffer sBuf = new StringBuffer();
				
				for (int i = 0; i <= share; i++) {
					String tempStr;
					
					if (i == share) {
						tempStr = shareText.substring(i * unitBind, shareText.length());
					} else {
						tempStr = shareText.substring(i * unitBind, (i + 1) * unitBind);
					}
					
					tempStr = sortText(tempStr);
					sBuf.append(tempStr);
				}
				
				sortedText = sBuf.toString();
			}
		}
		
		ParserResultVO resultVO = ParserResultVO.builder()
			.share(sortedText)
			.remain(remainText)
			.build();
		
		return resultVO;
	}
	
	/**
	 * 웹소스 추출 데이터 가공
	 * @param text
	 * @param division // 해당 값으로 HTML 태그 삭제여부 결정
	 * @return String
	 */
	public static String getParsingText(String text, String division) throws Exception {
		String result = null;
		
		try {
			result = delEmptySpace(text);
			
			if (WmpConstant.PARSER_PART.equals(division)) {
				result = delHtmlTag(result);
			}
			
			result = delSpecificWord(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		logger.info("[StringUtil.getParsingText] parsing source length : " + result.length());
		
		return result;
	}
	
	/**
	 * 개행, 탭, 빈공간 제거
	 * @param text
	 * @return String
	 * @throws Exception
	 */
	public static String delEmptySpace(String text) throws Exception {
		return text.replaceAll("(\r\n|\r|\n|\n\r|\\t|\\p{Z})", "");
	}
	
	/**
	 * html 태그 삭제,,, < > 괄호 사이값은 다 제거
	 * Stack 활용x
	 * @param text
	 * @return String
	 * @throws Exception
	 */
	public static String delHtmlTag(String text) throws Exception {
		String[] arr = delEmptySpace(text).split("");
		StringBuffer sBuf = new StringBuffer();
		boolean isDel = false;
		
		for(String txt : arr) {
			
			if (txt.equals("<")) {
				isDel = true;
			} else if (txt.equals(">")) {
				isDel = false;
				continue;
			} else {
				
				if (!isDel) {
					sBuf.append(txt);
				}
			}
		}
		
		return sBuf.toString();
	}
	
	/**
	 * 영문(대소문자),숫자 외 전부 삭제
	 * @param text
	 * @return String
	 * @throws Exception
	 */
	public static String delSpecificWord(String text) throws Exception {
		return delEmptySpace(text).replaceAll(PARSER_REGEX, "");
	}
	
	/**
	 * 숫자값인치 체크
	 * @param text
	 * @return true|false
	 */
	public static boolean isNum(String text) {
		return Pattern.matches(INT_REGEX, text);
	}
	
	/**
	 * 재귀 활용하여 가장 처음으로 문자열이 나올때를 구함 --> 이분탐색...비슷
	 * @param arr 대상 배열
	 * @param idx 기준점 index
	 * @param division  뒤로 확인하는지, 앞으로 확인하는지 여부
	 * @return
	 */
	private static int searchFirstText(String[] arr, int idx, int max, boolean division) {
		int mid = 0, last = arr.length;
		// 0번째 인덱스가 문자열이거나 마지막 인덱스가 숫자일 경우 바로 종료
		if (isNum(arr[last - 1])) {
			return last - 1;
		} else if (!isNum(arr[0])) {
			return -1;
		}
		
		if (division) {
			mid = (max + idx) / 2;
		} else {
			mid = idx / 2;
		}
		
		if (isNum(arr[mid])) {
			
			if (!isNum(arr[mid + 1])) {
				return mid;
			} else {
				division = true;
				return searchFirstText(arr, mid, max, division);
			}
		} else {
			
			if (isNum(arr[mid - 1])) {
				return mid;
			} else {
				division = false;
				max = mid;
				return searchFirstText(arr, mid, max, division);
			}
		}
	}
	
	/**
	 * 문자열 Comparator 활용하여 역순처리 진행
	 * 영문+숫자..순서 조합으로 진행될 수 있도록 index 구분으로 교차 진행
	 * @param shareText
	 * @return String
	 * @throws Exception
	 */
	public static String sortText(String shareText) throws Exception {
		String[] arr = shareText.split("");
		StringBuffer sBuf = new StringBuffer();
		Arrays.sort(arr, String.CASE_INSENSITIVE_ORDER);
		
		int divisionIdx = searchFirstText(arr, 0, arr.length, true);
		
		Arrays.sort(arr, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				int res = o1.compareToIgnoreCase(o2);
			    return (res == 0) ? o1.compareTo(o2) : res;
			}
		});
		
		int idx = 0, jdx = divisionIdx + 1;
		boolean order;
		
		if (jdx == arr.length) {
			order = false;
		} else {
			order = true;
		}
		
		while (true) {
			
			if (order) {
				sBuf.append(arr[jdx]);
				jdx ++;
				
				if (idx <= divisionIdx) {
					order = false;
				}
			} else {
				sBuf.append(arr[idx]);
				idx ++;
				
				if (jdx < arr.length) {
					order = true;
				}
			}
			
			if (idx > divisionIdx && jdx == arr.length) {
				break;
			}
		}
		
		return sBuf.toString();
	}
}

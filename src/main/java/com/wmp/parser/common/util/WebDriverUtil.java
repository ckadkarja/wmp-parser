package com.wmp.parser.common.util;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.logging.LogType;
import org.openqa.selenium.logging.LoggingPreferences;
import org.openqa.selenium.remote.CapabilityType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ApplicationContext;

import com.wmp.parser.common.consts.WmpConstant;

/**
 * =====================================================
 * @author | jaeyoung.yu
 * @mkdate | 2021. 05. 05.
 * @file   | WebDriverUtil.java
 * @desc   | 크롬 웹드라이버 유틸
 * =====================================================
 */
public class WebDriverUtil {
	
	/**
	 * 필요시 마다 생성 진행해야 함
	 * @return WebDriver
	 * @throws Exception
	 */
	public static WebDriver initWebDriver() throws Exception {
		String OS = System.getProperty("os.name").toLowerCase();
		
		if (OSUtil.isWindows(OS)) {
			System.setProperty(WmpConstant.WEBDRIVER_CHROME_DRIVER, "src/main/resources/driver/chromedriver.exe");
		} else { // window 외의 운영체제를 linux로 대체
			throw new Exception("OS Check..!");
		}
		
		System.setProperty(WmpConstant.WEBDRIVER_CHROME_ARGS, "--disable-logging"); 
	    System.setProperty(WmpConstant.WEBDRIVER_CHROME_SILENTOUTPUT, "true"); 
	    
		ChromeOptions options = new ChromeOptions();
		List<String> optionList = new ArrayList<String>();
		
		optionList.add("--no-sandbox");
		optionList.add("--disable-dev-shm-usage");
		optionList.add("--log-level=3");
		optionList.add("--silent");
		optionList.add("--disable-gpu");
		
		optionList.stream().forEach(o -> options.addArguments(o));
		
		// 크롬 브라우저 로그 (네트워크 포함) 관련 설정
		options.setHeadless(true);
		DesiredCapabilities cap = DesiredCapabilities.chrome();
		LoggingPreferences logPrefs = new LoggingPreferences();
		
		logPrefs.enable(LogType.BROWSER, Level.ALL);
	    logPrefs.enable(LogType.PERFORMANCE, Level.ALL);
		
	    cap.setCapability(CapabilityType.LOGGING_PREFS, logPrefs);
		
		options.merge(cap);
		
		return new ChromeDriver(options);
	}
}

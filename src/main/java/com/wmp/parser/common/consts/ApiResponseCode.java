package com.wmp.parser.common.consts;

import com.fasterxml.jackson.annotation.JsonFormat;

@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum ApiResponseCode {
	A_SUCCESS("01.001", 200, "OK"),
	A_PARAMETER_ERROR("01.002", 200, "parameter value check"),
	A_URL_ERROR("01.003", 200, "url value check"),
	A_PARSING_ERROR("01.004", 200, "parsing error"); // 예기치 못한 에러 상황일 시 사용
	
	private int httpStatus;
    private String code;
    private String reason;

    ApiResponseCode(String code, int httpStatus, String reason) {
        this.code = code;
        this.httpStatus = httpStatus;
        this.reason = reason;
    }

    public String getCode() {
        return code;
    }

    public int getHttpStatus() {
        return httpStatus;
    }

    public String getReason() {
        return reason;
    }
    
    @Override
    public String toString() {
        return name() + ": " + code + ":" + httpStatus;
    }
}

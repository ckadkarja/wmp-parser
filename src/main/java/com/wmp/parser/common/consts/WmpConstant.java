package com.wmp.parser.common.consts;

public class WmpConstant {
	/** Parser **/
	public final static String PARSER_ALL = "ALL";
	public final static String PARSER_PART = "PART";
	
	/** 크롬드라이버 **/
	public final static String WEBDRIVER_CHROME_DRIVER = "webdriver.chrome.driver";
	public final static String WEBDRIVER_CHROME_ARGS = "webdriver.chrome.args";
	public final static String WEBDRIVER_CHROME_SILENTOUTPUT = "webdriver.chrome.silentOutput";
	public final static String OS_NAME = "os.name";
}

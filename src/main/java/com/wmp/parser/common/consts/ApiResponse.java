package com.wmp.parser.common.consts;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@Builder
public class ApiResponse {
    private int code; // HttpStatus status
    private Object apiData; // api data
    private String message; // api message
}

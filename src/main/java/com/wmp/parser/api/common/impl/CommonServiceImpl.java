package com.wmp.parser.api.common.impl;

import org.openqa.selenium.WebDriver;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import com.wmp.parser.api.common.CommonService;
import com.wmp.parser.common.util.StringUtil;
import com.wmp.parser.common.util.WebDriverUtil;

@Service("commonService")
public class CommonServiceImpl implements CommonService {
	private static final Logger logger = LoggerFactory.getLogger(CommonServiceImpl.class);
	
	/**
	 * URL 기반으로 웹사이트 크롤링 진행
	 * 크롬드라이버 & 셀레니움 활용
	 * @param url
	 * @return String
	 */
	@Override
	public String getWebSource(String url) throws Exception {
		WebDriver webDriver = WebDriverUtil.initWebDriver();
		String pageSource = "";
		
		try {
			
			if (!StringUtil.isUrlFormat(url)) {
				throw new Exception("[getWebSource url format error] : url format check");
			}
			
			webDriver.get(url);
			pageSource = webDriver.getPageSource();
			
			logger.info("[pageSource.length] : " + pageSource.length());
		} catch(Exception e) {
			logger.error("[getWebSource error] : " + e.getMessage());
		//	e.printStackTrace();
		} finally {
			
			if (webDriver != null) {
				webDriver.quit();
			}
		}
		
		return pageSource;
	}

}

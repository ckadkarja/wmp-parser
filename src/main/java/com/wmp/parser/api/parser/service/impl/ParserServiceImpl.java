package com.wmp.parser.api.parser.service.impl;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.wmp.parser.api.common.CommonService;
import com.wmp.parser.api.parser.service.ParserService;
import com.wmp.parser.beans.ParserResultVO;
import com.wmp.parser.common.consts.ApiResponse;
import com.wmp.parser.common.consts.ApiResponseCode;
import com.wmp.parser.common.util.StringUtil;

@Service("parserService")
public class ParserServiceImpl implements ParserService {
	private static final Logger logger = LoggerFactory.getLogger(ParserServiceImpl.class);
	
	@Autowired
	private CommonService commonService;
	
	@Override
	public ApiResponse getParsingData(Map<String, Object> paramMap) throws Exception {
		String url = (String) paramMap.get("url");
		String textDivision = (String) paramMap.get("textDivision");
		String bindType = (String) paramMap.get("bindType");
		int unitBind = 0;
		ApiResponse apiRes = null;
		
		try {
			unitBind = Integer.parseInt((String) paramMap.get("unitBind"));
		} catch (Exception e) {
			logger.error("getParsingData Integer parsing error : " + e.getMessage());
			
			apiRes = ApiResponse.builder()
					.code(ApiResponseCode.A_PARAMETER_ERROR.getHttpStatus())
					.message(ApiResponseCode.A_PARAMETER_ERROR.getReason())
					.build();
			
			return apiRes;
		}
		
		String webSource = commonService.getWebSource(url);
		String parsingSource = StringUtil.getParsingText(webSource, textDivision);
		
		ParserResultVO resultVO = StringUtil.getExtractedValue(parsingSource, unitBind, bindType);
		
		apiRes = ApiResponse.builder()
				.apiData(resultVO)
				.code(ApiResponseCode.A_SUCCESS.getHttpStatus())
				.build();
		
		return apiRes;
	}

}

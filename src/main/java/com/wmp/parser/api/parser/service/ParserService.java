package com.wmp.parser.api.parser.service;

import java.util.Map;

import com.wmp.parser.common.consts.ApiResponse;

public interface ParserService {

	ApiResponse getParsingData(Map<String, Object> paramMap) throws Exception;
	
}

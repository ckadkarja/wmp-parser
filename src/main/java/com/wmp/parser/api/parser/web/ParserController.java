package com.wmp.parser.api.parser.web;

import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.wmp.parser.api.parser.service.ParserService;
import com.wmp.parser.common.consts.ApiResponse;
import com.wmp.parser.common.consts.ApiResponseCode;

@Controller
public class ParserController {
	private static final Logger logger = LoggerFactory.getLogger(ParserController.class);

	@Autowired
	private ParserService parserService;
	
	@RequestMapping("/")
	public String parserHome() throws Exception {
		return "parser/parser";
	}
	
	@RequestMapping(value = "/parser/getParsingData.json", 
			produces = "application/json; charset=UTF-8", 
			method = RequestMethod.POST)
	@ResponseBody
	public ResponseEntity<ApiResponse> getParsingData(@RequestBody Map<String, Object> paramMap) {
		ApiResponse apiRes = null;
		
		try {
			apiRes = parserService.getParsingData(paramMap);
		} catch (Exception e) {
			logger.error("[ParserController.getParsingData error] : " + e.getMessage());
			apiRes.setCode(ApiResponseCode.A_PARSING_ERROR.getHttpStatus());
			apiRes.setMessage(ApiResponseCode.A_PARSING_ERROR.getReason());
		}
		
		return new ResponseEntity<ApiResponse>(apiRes, HttpStatus.valueOf(apiRes.getCode()));
	}
}

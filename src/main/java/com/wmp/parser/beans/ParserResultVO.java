package com.wmp.parser.beans;

import lombok.Builder;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@Builder
@ToString
public class ParserResultVO {
	private String share;
	private String remain;
}

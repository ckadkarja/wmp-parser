package com.wmp.parser;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.servlet.support.SpringBootServletInitializer;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan("com.wmp.parser.*")
@SpringBootApplication
public class WmpParserApplication extends SpringBootServletInitializer {

	@Override
	protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		return application.sources(WmpParserApplication.class);
	}

	public static void main(String[] args) {
		SpringApplication.run(WmpParserApplication.class, args);
	}

}

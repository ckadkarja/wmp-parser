/**
 * =========================================
 * 작성자		| 유재영
 * 최초생성일	| 2021.05.05
 * 파일명		| common.js
 * 설명		| common 공통 JS
 * =========================================
 */
let common = {
	
	/**
	 * @Author 유재영
	 * =========================================
	 * 공통 ajax 호출
	 * =========================================
	 * 전달값		| Object (4개 Property)
	 * url		| 호출 url
	 * param	| 전달값 
	 * type		| POST or GET
	 * onSuccess| 성공 시 실행 함수
	 * 
	 * * contentType 과 dataType 은 고정 !!
	 * =========================================
	 */
	callAjax		: function(url, param, type, onSuccess) {
		// argument 체크! --> url
		if (this.isNonValidVal(url)) {
			alert("url path check !!");
			return false;
		}
		
		if (type == "GET" || type == "get") {
			this.getJson(url, param, onSuccess);
		} else if (type == "POST" || type == "post") {
			this.postJson(url, param, onSuccess);
		} else {
			alert("Submit type check !!");
			return false;
		}
	},
	
	/**
	 * @Author 유재영
	 * ======================================================
	 * ajax type : GET 일 경우 
	 * --> 현재 post 와는 contentType 차이
	 * ======================================================
	 */
	getJson			: function(url, param, onSuccess) {
		
		$.ajax({
			url			: url,
			data		: param,
			type		: "GET",
			dataType    : "json",
			cache		: false,
			async		: false,
			traditional	: true,
			success		: function(result) {
				
				if (typeof onSuccess === "function") {
					onSuccess(result);
				}
			},
			error	: function(errMsg, xhr) {
				console.log("ajax error: " , errMsg, " xhr : ", xhr);
			}
		});
	},
	
	/**
	 * @Author 유재영
	 * ======================================================
	 * ajax type : POST 일 경우 
	 * ======================================================
	 */
	postJson		: function(url, param, onSuccess) {
		
		$.ajax({
			url			: url,
			data		: JSON.stringify(param),
			type		: 'POST',
			dataType    : 'json',
			contentType : 'application/json; charset=UTF-8',
			cache		: false,
			async		: false,
			traditional	: true,
			success		: function(result) {
				
				if (typeof onSuccess === "function") {
					onSuccess(result);
				}
			},
			error	: function(errMsg, xhr) {
				console.log("ajax error: " , errMsg, " xhr : ", xhr);
			}
		});
	},
	
	/**
	 * @Author 유재영
	 * ======================================================
	 * 밸리데이션		| 빈 값(null, "", undefined) 체크
	 * return		| true / false
	 * 빈 값시 true (유효하지 않은 값)
	 * ======================================================
	 */
	isNonValidVal	: function(param) {
		return (param === null || param === undefined || param === "" || param === false);
	}
};


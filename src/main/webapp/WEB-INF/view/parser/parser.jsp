<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<html lang="ko">
	<head>
		<!-- App css -->
	    <link href="${pageContext.request.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
	    <style>
	    	.form-area {
	    		margin: 10px;
	    		border: 2px solid #6f6f6f;
	    	}
	    	
	    	.shareTarget, .remainTarget {
	    		word-break: break-all;
	    		width: 300px;
	    	}
	    </style>
	    <!-- jQuery  -->
	    <script type="text/javascript" src="${pageContext.request.contextPath}/js/jquery.min.js"></script>
	    <script type="text/javascript" src="${pageContext.request.contextPath}/js/common.js"></script>
	    <script type="text/javascript">
			$(document).ready(function() {
				
				let Parser = {
					PARSING_SUCCESS_MESSAGE : "추출 작업에 성공하였습니다.",
					PARSING_FAIL_MESSAGE : "추출 작업에 실패하였습니다.",
						
					// url 형식 체크 --> 리펙토링 필요
					isUrlType : function(url) {
						let regex = /(http|https):\/\/(\w+:{0,1}\w*@)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%@!\-\/]))?/;
						return regex.test(url);
					},
					
					isNum : function(text) {
						let regex = /^[0-9]*$/;
						return regex.test(text);
					}
				}
				
				// 출력버튼 이벤트 리스너
				$("#submitBtn").click(function(e) {
					let paramObj = {
						"url" : $("#url").val(),
						"textDivision" : $("#textDivision").val(),
						"unitBind" : $("#unitBind").val(),
						"bindType" : $("#bindType").val()
					}
					
					if (common.isNonValidVal(paramObj.url)) {
						alert("크롤링 대상 도메인 URL 입력이 필요합니다.");
						$("#url").focus();
						return false;
					} else {
						
						if (!Parser.isUrlType(paramObj.url)) {
							alert("URL 형식 확인이 필요합니다.");
							$("#url").val("");
							$("#url").focus();
							return false;
						}
					}
					
					if (common.isNonValidVal(paramObj.unitBind)) {
						alert("출력 단위 묶음을 입력이 필요합니다.");
						$("#unitBind").focus();
						return false;
					} else {
						
						if (!Parser.isNum(paramObj.unitBind)) {
							alert("숫자만 입력가능합니다.");
							$("#unitBind").val("");
							$("#unitBind").focus();
							return false;
						}
					}
					
					common.callAjax("/parser/getParsingData.json", paramObj, "post", function(result) {
						console.log("result : ", result);
						
						if (!common.isNonValidVal(result.message) ||
								result.code != "200") {
							alert(Parser.PARSING_FAIL_MESSAGE);
							return false;
						} else {
							alert(Parser.PARSING_SUCCESS_MESSAGE);
						}
						
						let resultObj = result.apiData;
						
						$(".shareTarget").text(resultObj.share);
					//	console.log(resultObj.share.length);
						$(".remainTarget").text(resultObj.remain);
					//	console.log(resultObj.remain.length);
					});
				})
			});
		</script>
	</head>
	<body>
		<div class="col-lg-2 form-area">
			<div class="col-sm-6">
				[입력]</br>
			</div>
			<div class="col-sm-6">
				URL <input type="text" name="url" id="url" />
			</div>
			<div class="col-sm-6">
				Type 
				<select name="textDivision" id="textDivision">
					<option value="PART">HTML 태그 제외</option>
					<option value="ALL">Text 전체</option>
				</select>
			</div>
			<div class="col-sm-6">
				<span style="color:red;">* 소팅기준</span> 
				<select name="bindType" id="bindType">
					<option value="ALL">전체에서 소팅</option>
					<option value="PART">묶음내 소팅</option>
				</select>
			</div>
			<div class="col-sm-6">
				출력 단위 묶음 <input type="text" name="unitBind" id="unitBind" />
			</div>
			<div class="col-sm-6">
				</br>
				<input type="button" name="submitBtn" id="submitBtn" value="출력" />
			</div>
			<div class="col-sm-6">
				[출력]
			</div>
			<div class="col-sm-6">
				몫 : 
				<div class="col-sm-12">
					<p class="shareTarget">
					</p>
				</div>
			</div>
			<div class="col-sm-6">
				나머지 : 
				<div class="col-sm-12">
					<p class="remainTarget">
					</p>
				</div>
			</div>
		</div>
	</body>	
</html>

package com.wmp.parser.common.util;

import java.util.Arrays;
import java.util.Comparator;
import java.util.regex.Pattern;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import com.wmp.parser.common.consts.WmpConstant;

/**
 * 문자열 치환 테스트
 * @author jaeyoung.yu
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class ParserTest {
	private final String REGEX = "[^0-9a-zA-Z]";
	private final String INT_REGEX = "[0-9]";
	private final String INT_TEXT = "123452364"; // 숫자로만 이루어진 도메인 테스트 하기 위해 작성
	private final String TEXT = "<html lang=\"ko\"><head>\r\n" + 
			"\r\n" + 
			"\r\n" + 
			"    <meta charset=\"UTF-8\">\r\n" + 
			"    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\r\n" + 
			"\r\n" + 
			"    <meta name=\"viewport\" content=\"width=device-width, initial-scale=2,maximum-scale=1.0\">\r\n" + 
			"\r\n" + 
			"    <title>DUNDAM</title>\r\n" + 
			"\r\n" + 
			"    <link href=\"https://fonts.googleapis.com/css?family=Barlow:100,200,300,400,500,600,700,800,900|Nanum+Gothic|Nanum+Myeongjo:400,700,800|Noto+Sans+KR:100,300,400,500,700,900|Noto+Serif+KR:200,300,400,500,600,700,900|Staatliches\" rel=\"stylesheet\">\r\n" + 
			"    <link rel=\"stylesheet\" href=\"https://use.fontawesome.com/releases/v5.8.1/css/all.css\" integrity=\"sha384-50oBUHEmvpQ+1lW4y57PTFmhCaXp0ML5d60M1M7uH2+nqUivzIebhndOJK28anvf\" crossorigin=\"anonymous\">\r\n" + 
			"    <link rel=\"stylesheet\" href=\"css/font-awesome.css\">\r\n" + 
			"\r\n" + 
			"    <link rel=\"stylesheet\" href=\"css/reset.css\">\r\n" + 
			"    <link rel=\"stylesheet\" href=\"css/DUNDAM_2020.css?ver=1.03\">\r\n" + 
			"    <link rel=\"stylesheet\" href=\"css/mobile.css?ver=1.01\">\r\n" + 
			"    <link rel=\"stylesheet\" href=\"css/JT_PC.css?ver=1.10\">\r\n" + 
			"    \r\n" + 
			"    \r\n" + 
			"\r\n" + 
			"    <script src=\"https://pagead2.googlesyndication.com/pagead/js/r20210429/r20190131/reactive_library.js\"></script><script src=\"https://www.googletagservices.com/activeview/js/current/osd.js\"></script><script src=\"https://partner.googleadservices.com/gampad/cookie.js?domain=dundam.xyz&amp;callback=_gfp_s_&amp;client=ca-pub-5271261855072426\"></script><script src=\"https://pagead2.googlesyndication.com/pagead/js/r20210429/r20190131/rum.js\"></script><script src=\"https://pagead2.googlesyndication.com/pagead/js/r20210429/r20190131/show_ads_impl.js\" id=\"google_shimpl\"></script><script src=\"js/jquery.min_1.12.4.js\"></script>\r\n" + 
			"\r\n" + 
			"<meta http-equiv=\"origin-trial\" content=\"A+b/H0b8RPXNaJgaNFpO0YOFuGK6myDQXlwnJB3SwzvNMfcndat4DZYMrP4ClJIzYWo3/yP2S+8FTZ/lpqbPAAEAAABueyJvcmlnaW4iOiJodHRwczovL2ltYXNkay5nb29nbGVhcGlzLmNvbTo0NDMiLCJmZWF0dXJlIjoiVHJ1c3RUb2tlbnMiLCJleHBpcnkiOjE2MjYyMjA3OTksImlzVGhpcmRQYXJ0eSI6dHJ1ZX0=\"><meta http-equiv=\"origin-trial\" content=\"A9ZgbRtm4pU3oZiuNzOsKcC8ppFSZdcjP2qYcdQrFKVzkmiWH1kdYY1Mi9x7G8+PS8HV9Ha9Cz0gaMdKsiVZIgMAAAB7eyJvcmlnaW4iOiJodHRwczovL2RvdWJsZWNsaWNrLm5ldDo0NDMiLCJmZWF0dXJlIjoiVHJ1c3RUb2tlbnMiLCJleHBpcnkiOjE2MjYyMjA3OTksImlzU3ViZG9tYWluIjp0cnVlLCJpc1RoaXJkUGFydHkiOnRydWV9\"><meta http-equiv=\"origin-trial\" content=\"AxL6oBxcpn5rQDPKSAs+d0oxNyJYq2/4esBUh3Yx5z8QfcLu+AU8iFCXYRcr/CEEfDnkxxLTsvXPJFQBxHfvkgMAAACBeyJvcmlnaW4iOiJodHRwczovL2dvb2dsZXRhZ3NlcnZpY2VzLmNvbTo0NDMiLCJmZWF0dXJlIjoiVHJ1c3RUb2tlbnMiLCJleHBpcnkiOjE2MjYyMjA3OTksImlzU3ViZG9tYWluIjp0cnVlLCJpc1RoaXJkUGFydHkiOnRydWV9\"><meta http-equiv=\"origin-trial\" content=\"A9KPtG5kl3oLTk21xqynDPGQ5t18bSOpwt0w6kGa6dEWbuwjpffmdUpR3W+faZDubGT+KIk2do0BX2ca16x8qAcAAACBeyJvcmlnaW4iOiJodHRwczovL2dvb2dsZXN5bmRpY2F0aW9uLmNvbTo0NDMiLCJmZWF0dXJlIjoiVHJ1c3RUb2tlbnMiLCJleHBpcnkiOjE2MjYyMjA3OTksImlzU3ViZG9tYWluIjp0cnVlLCJpc1RoaXJkUGFydHkiOnRydWV9\"><meta http-equiv=\"origin-trial\" content=\"A3HucHUo1oW9s+9kIKz8mLkbcmdaj5lxt3eiIMp1Nh49dkkBlg1Fhg4Fd/r0vL69mRRA36YutI9P/lJUfL8csQoAAACFeyJvcmlnaW4iOiJodHRwczovL2RvdWJsZWNsaWNrLm5ldDo0NDMiLCJmZWF0dXJlIjoiQ29udmVyc2lvbk1lYXN1cmVtZW50IiwiZXhwaXJ5IjoxNjI2MjIwNzk5LCJpc1N1YmRvbWFpbiI6dHJ1ZSwiaXNUaGlyZFBhcnR5Ijp0cnVlfQ==\"><meta http-equiv=\"origin-trial\" content=\"A0OysezhLoCRYomumeYlubLurZTCmsjTb087OvtCy95jNM65cfEsbajrJnhaGwiTxhz38ZZbm+UhUwQuXfVPTg0AAACLeyJvcmlnaW4iOiJodHRwczovL2dvb2dsZXN5bmRpY2F0aW9uLmNvbTo0NDMiLCJmZWF0dXJlIjoiQ29udmVyc2lvbk1lYXN1cmVtZW50IiwiZXhwaXJ5IjoxNjI2MjIwNzk5LCJpc1N1YmRvbWFpbiI6dHJ1ZSwiaXNUaGlyZFBhcnR5Ijp0cnVlfQ==\"><meta http-equiv=\"origin-trial\" content=\"AxoOxdZQmIoA1WeAPDixRAeWDdgs7ZtVFfH2y19ziTgD1iaHE5ZGz2UdSjubkWvob9C5PrjUfkWi4ZSLgWk3Xg8AAACLeyJvcmlnaW4iOiJodHRwczovL2dvb2dsZXRhZ3NlcnZpY2VzLmNvbTo0NDMiLCJmZWF0dXJlIjoiQ29udmVyc2lvbk1lYXN1cmVtZW50IiwiZXhwaXJ5IjoxNjI2MjIwNzk5LCJpc1N1YmRvbWFpbiI6dHJ1ZSwiaXNUaGlyZFBhcnR5Ijp0cnVlfQ==\"><meta http-equiv=\"origin-trial\" content=\"A7+rMYR5onPnACrz+niKSeFdH3xw1IyHo2AZSHmxrofRk9w4HcQPMYcpBUKu6OQ6zsdxf4m/vqa6tG6Na4OLpAQAAAB4eyJvcmlnaW4iOiJodHRwczovL2ltYXNkay5nb29nbGVhcGlzLmNvbTo0NDMiLCJmZWF0dXJlIjoiQ29udmVyc2lvbk1lYXN1cmVtZW50IiwiZXhwaXJ5IjoxNjI2MjIwNzk5LCJpc1RoaXJkUGFydHkiOnRydWV9\"><link rel=\"preload\" href=\"https://adservice.google.co.kr/adsid/integrator.js?domain=dundam.xyz\" as=\"script\"><script type=\"text/javascript\" src=\"https://adservice.google.co.kr/adsid/integrator.js?domain=dundam.xyz\"></script><link rel=\"preload\" href=\"https://adservice.google.com/adsid/integrator.js?domain=dundam.xyz\" as=\"script\"><script type=\"text/javascript\" src=\"https://adservice.google.com/adsid/integrator.js?domain=dundam.xyz\"></script><link rel=\"preload\" href=\"https://adservice.google.co.kr/adsid/integrator.js?domain=dundam.xyz\" as=\"script\"><script type=\"text/javascript\" src=\"https://adservice.google.co.kr/adsid/integrator.js?domain=dundam.xyz\"></script><link rel=\"preload\" href=\"https://adservice.google.com/adsid/integrator.js?domain=dundam.xyz\" as=\"script\"><script type=\"text/javascript\" src=\"https://adservice.google.com/adsid/integrator.js?domain=dundam.xyz\"></script></head>\r\n" + 
			"\r\n" + 
			"<body style=\"height: 100%\" aria-hidden=\"false\">\r\n" + 
			"    \r\n" + 
			"    \r\n" + 
			"    <div id=\"header\">\r\n" + 
			"      <!--로고 -->\r\n" + 
			"  \r\n" + 
			"      <div class=\"logo\" name=\"로고\"><a href=\"/newIndex\"><img src=\"img/DUN-logo.png\"></a>\r\n" + 
			"      </div>\r\n" + 
			"      <!--//로고 -->\r\n" + 
			"  \r\n" + 
			"      <!-- m-m ='메인메뉴' !-->\r\n" + 
			"      <div class=\"m-menu\" name=\"메인메뉴\">\r\n" + 
			"          <ul class=\"m-m\">\r\n" + 
			"              <menu>\r\n" + 
			"                  <menuitem onclick=\"location.href = '/newIndex';\"><a href=\"/newIndex\" style=\"color: rgb(0, 0, 0);\">캐릭터검색</a></menuitem> \r\n" + 
			"                  <menuitem onclick=\"location.href = '/buffer';\"><a href=\"/buffer\">버프랭킹</a></menuitem> \r\n" + 
			"                  <menuitem onclick=\"location.href = '/dealer';\"><a href=\"/dealer\">딜러랭킹</a></menuitem> \r\n" + 
			"                  <menuitem onclick=\"location.href = '/party';\"><a href=\"/party\">가상파티</a></menuitem>\r\n" + 
			"                  <menuitem onclick=\"location.href = '/statistic';\"><a href=\"/statistic\">인구수</a></menuitem>\r\n" + 
			"                  <menuitem onclick=\"location.href = '/detail';\"><a href=\"/detail\">상세랭킹</a></menuitem>\r\n" + 
			"              </menu> \r\n" + 
			"              \r\n" + 
			"              \r\n" + 
			"              <div class=\"sliding-bar\" name=\"메뉴 슬라이드\" style=\"top: 100.219px; left: 1.29688px; width: 181px; height: 45px; opacity: 1;\"></div>\r\n" + 
			"          </ul>\r\n" + 
			"      </div>";
	
	
	/*
	@BeforeTestClass
	public static void setText() {

	}
	*/
	
	/**
	 * 전체 공백 제거 정규식
	 * @throws Exception
	 */
	@Test
	public void delEmptySpace() throws Exception {
		String result = this.TEXT.replaceAll("(\r\n|\r|\n|\n\r|\\t|\\p{Z})", "");
		System.out.println("[delEmptySpace] : " + result);
	}
	
	private String delEmptySpace(String text) throws Exception {
		return text.replaceAll("(\r\n|\r|\n|\n\r|\\t|\\p{Z})", "");
	}
	
	/**
	 * html 태그 삭제
	 * @throws Exception
	 */
	@Test
	public void delHtmlTag() throws Exception {
		String[] arr = delEmptySpace(this.TEXT).split("");
		StringBuffer sBuf = new StringBuffer();
		boolean isDel = false;
		
		for(String text : arr) {
			
			if (text.equals("<")) {
				isDel = true;
			} else if (text.equals(">")) {
				isDel = false;
				continue;
			} else {
				
				if (!isDel) {
					sBuf.append(text);
				}
			}
		}
		
		System.out.println("[delHtmlTag] : " + sBuf.toString());
	}
	
	private String delHtmlTag(String text) throws Exception {
		String[] arr = delEmptySpace(text).split("");
		StringBuffer sBuf = new StringBuffer();
		boolean isDel = false;
		
		for(String txt : arr) {
			
			if (txt.equals("<")) {
				isDel = true;
			} else if (txt.equals(">")) {
				isDel = false;
				continue;
			} else {
				
				if (!isDel) {
					sBuf.append(txt);
				}
			}
		}
		
		return sBuf.toString();
	}
	
	
	/**
	 * 출력열 (대소구분)영문, 숫자 외 삭제
	 * @throws Exception
	 */
	@Test
	public void delSpecificWord() throws Exception {
		String result = delEmptySpace(this.TEXT).replaceAll(this.REGEX, "");
		System.out.println("[delSpecificWord] : " + result);
	}
	
	private String delSpecificWord(String text) throws Exception {
		return delEmptySpace(text).replaceAll(this.REGEX, "");
	}
	
	private String getParsingText(String text, String division) throws Exception {
		String result = null;
		
		try {
			result = delEmptySpace(text);
			
			if (WmpConstant.PARSER_PART.equals(division)) {
				result = delHtmlTag(result);
			}
			
			result = delSpecificWord(result);
		} catch (Exception e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	
	/**
	 * 재귀 활용하여 가장 처음으로 문자열이 나올때를 구함 --> 이분탐색...비스무리
	 * @param arr 대상 배열
	 * @param idx 기준점 index
	 * @param division  뒤로 확인하는지, 앞으로 확인하는지 여부
	 * @return
	 */
	private int searchFirstText(String[] arr, int idx, int max, boolean division) {
		int mid = 0, last = arr.length;
		// 0번째 인덱스가 문자열이거나 마지막 인덱스가 숫자일 경우 바로 종료
		if (isNum(arr[last - 1])) {
			return last - 1;
		} else if (!isNum(arr[0])) {
			return -1;
		}
		
		if (division) {
			mid = (max + idx) / 2;
		} else {
			mid = idx / 2;
		}
		
		if (isNum(arr[mid])) {
			
			if (!isNum(arr[mid + 1])) {
				return mid;
			} else {
				division = true;
				return searchFirstText(arr, mid, max, division);
			}
		} else {
			
			if (isNum(arr[mid - 1])) {
				return mid;
			} else {
				division = false;
				max = mid;
				return searchFirstText(arr, mid, max, division);
			}
		}
	}
	
	/**
	 * sort 자체 기능 활용 및 Comparator 활용하여 대소문자 소팅 진행
	 * @throws Exception
	 */
	@Test
	public void sortText() throws Exception {
		String[] arr = "a81fdgdf32147zZG92".split("");
		StringBuffer sBuf = new StringBuffer();
		Arrays.sort(arr, String.CASE_INSENSITIVE_ORDER);
		
		int divisionIdx = searchFirstText(arr, 0, arr.length, true);
		
		Arrays.sort(arr, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				int res = o1.compareToIgnoreCase(o2);
			    return (res == 0) ? o1.compareTo(o2) : res;
			}
		});
		
		int idx = 0, jdx = divisionIdx + 1;
		boolean order;
		
		if (jdx == arr.length) {
			order = false;
		} else {
			order = true;
		}
		
		while (true) {
			
			if (order) {
				sBuf.append(arr[jdx]);
				jdx ++;
				
				if (idx <= divisionIdx) {
					order = false;
				}
			} else {
				sBuf.append(arr[idx]);
				idx ++;
				
				if (jdx < arr.length) {
					order = true;
				}
			}
			
			if (idx > divisionIdx && jdx == arr.length) {
				break;
			}
		}
		
		System.out.println("[sortText] : " + sBuf.toString());
	}
	
	private String sortText(String shareText) throws Exception {
		String[] arr = shareText.split("");
		StringBuffer sBuf = new StringBuffer();
		Arrays.sort(arr, String.CASE_INSENSITIVE_ORDER);
		
		int divisionIdx = searchFirstText(arr, 0, arr.length, true);
		
		Arrays.sort(arr, new Comparator<String>() {

			@Override
			public int compare(String o1, String o2) {
				int res = o1.compareToIgnoreCase(o2);
			    return (res == 0) ? o1.compareTo(o2) : res;
			}
		});
		
		int idx = 0, jdx = divisionIdx + 1;
		boolean order;
		
		if (jdx == arr.length) {
			order = false;
		} else {
			order = true;
		}
		
		while (true) {
			
			if (order) {
				sBuf.append(arr[jdx]);
				jdx ++;
				
				if (idx <= divisionIdx) {
					order = false;
				}
			} else {
				sBuf.append(arr[idx]);
				idx ++;
				
				if (jdx < arr.length) {
					order = true;
				}
			}
			
			if (idx > divisionIdx && jdx == arr.length) {
				break;
			}
		}
		
		return sBuf.toString();
	}
	
	private boolean isNum(String text) {
		return Pattern.matches(this.INT_REGEX, text);
	}
	
	/**
	 * 
	 * @throws Exception
	 */
	@Disabled
	@Test
	public void calWord() throws Exception {
		int randomNum = 4;
		
		int textLength = this.TEXT.length();
		int remain = textLength % randomNum;

		System.out.println("bindWord : " + this.TEXT.substring(0, (textLength - remain)));
		System.out.println("remainWord : " + this.TEXT.substring((textLength - remain), textLength));
	}
	
	// 문자열 길이만큼 for문 실행
	@Test
	public void initStrArray() throws Exception {
		String aaa = "FAsfasdfasxvbsdhagasfd";
		int num = 4, aLength = aaa.length();
		int share = aLength / num;
		StringBuffer sBuf = new StringBuffer();
		
		for (int i = 0; i <= share; i++) {
			String tempStr;
			
			if (i == share) {
				tempStr = aaa.substring(i * num, aaa.length());
			} else {
				tempStr = aaa.substring(i * num, (i + 1) * num);
			}
			tempStr = sortText(tempStr);
			sBuf.append(tempStr);
		}
		
		System.out.println("[initStrArray] : " + sBuf.toString());
	}
	
	// 전체 문자열채로 소팅
	@Test
	public void testAll() {
		String parsingText;
		
		try {
		//	parsingText = getParsingText(this.TEXT, WmpConstant.PARSER_ALL);
			parsingText = getParsingText(this.TEXT, WmpConstant.PARSER_PART);
			System.out.println("[testAll] parsingText : " + parsingText);
			
			int textLength = parsingText.length(),
				unitBind = 2;
			int remainLength = textLength % unitBind;

			String shareText = parsingText.substring(0, (textLength - remainLength));
			String sortedText = "";
			
			if (textLength != remainLength) {
				sortedText = sortText(shareText);
			}
			
			System.out.println("[testAll] sortedText : " + sortedText);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}

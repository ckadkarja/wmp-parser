package com.wmp.parser.common.util;

import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.openqa.selenium.WebDriver;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit.jupiter.SpringExtension;

/**
 * 웹드라이버 테스트
 * @author jaeyoung.yu
 */
@ExtendWith(SpringExtension.class)
@SpringBootTest
public class WebDriverTest {
	private String testUrl = "https://www.naver.com";
	
	
	@Test
	public void getHtmlElement() throws Exception {
		WebDriver webDriver = WebDriverUtil.initWebDriver();
		
		webDriver.get(this.testUrl);
		System.out.println("pageSource : " + webDriver.getPageSource());
		
		if (webDriver != null) {
			webDriver.quit();
		}
	}
}

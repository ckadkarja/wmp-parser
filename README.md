# Springboot project 생성
1) STS 에서 start.spring.io를 이용하여 프로젝트 생성

2) 생성 옵션 : 
* type(gradle project)
* pakaging(war)
* java version(8)
* group(com.wmp.parser)
* springBootVersion(2.4.6.Release)  

3) 설정파일 : 
* /src/main/resources/config/application.properties
  
4) dependencies : 
* org.springframework.boot:spring-boot-starter-web
* org.projectlombok:lombok
```
1) 별도 설치 : 
org.seleniumhq.selenium:selenium-java
javax.servlet:jstl
org.apache.tomcat.embed:tomcat-embed-jasper
```

5) System Requirements
* Java 8
* Maven
* Spring Framework 5.0.2.RELEASE or above.
* Tomcat Version 8.0.64 or above.

# API 사용
1) URI : /parser/getParsingData.json
```
1) @Controller : 
* JSP(Front) 를 포함한 프로젝트로써 별도의 RestTemplate 활용한 Rest API 작성 미진행
* Rest API 구현으로 마이그레이션 시 API Conventions 수정해야함.
  - CamelCase 로 작성
  - API URI : 두단어 이상 사용되지 않도록 분리 필요 / 불가피한 경우 "-" 사용

2) @RequestMapping : 
* URL을 컨트롤러의 메서드와 매핑할 때 사용하는 스프링 프레임워크의 어노테이션
  
3) ApiResponseCode 타입으로 결과 리턴
```  

# lombok 적용
* @Getter, @Setter, @ToString
* dependencies : org.projectlombok:lombok
* sample : src/main/java/com.wmp.parser.beans/ParserResultVO.java
* sts : lombok plugin 설치(/WEB-INF/lib/lombok.jar) 활용


# Junit 활용한 테스트 진행
* 크롬 웹드라이버 객체 생성 테스트 및 Element(pageSource) 가져오기 테스트
* 문자열 파싱 테스트 진행
```
1. 빈공간, 개행, 탭 문자 제거
2. HTML 태그 제거
3. 영문(대소문자), 숫자 외 문자 제거
4. 문자열 소팅
   - 소팅을 위한 중간 탐색로직 추가
   - Comparator 활용한 문자열 비교 진행
```

# 추가 검토가 필요한 부분
* URL Regex : URL Regex javascript 및 java 로직 좀 더 검토 필요
* 실제 웹서비스 구현시 @ControllerAdvice 등을 활용한 Exception 관리 필요
* 비즈니스 로직 개선
```
 1. 크롬 웹드라이버 생성주기 관리를 위해 호출 시마다 생성하게끔 진행함. --> 최초 로딩 시 느린 부분 개선
 2. 문자열 파싱 시 최초 Arrays.sort(t, String.CASE_INSENSITIVE_ORDER) 소팅 진행 후 한번더 가공을 진행하였으며 좀 더 개선의 여지가 있는지 확인 필요 등
```
  

